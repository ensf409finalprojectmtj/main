package main.FrontEnd;

import main.data.*;

import javax.swing.*;
import java.awt.*;

/**
 * A class that is a Frame for looking at content in the course. Enters this GUI after selecting a course.
 * @author Matthew Wiens
 * @author Taylor Huang
 * @author Jeffrey Layton
 */
public class StudentCourseWindow extends GUIWindow {
	
	/**
	 *  Container to select assignments in the course
	 */
  private DefaultListModel<String> assignmentModel = new DefaultListModel<>();
  
	/**
	 *  Field to display and select assignments
	 */
  private JList<String> assignmentList;

	/**
	 * Button to download assignment selected
	 */
  private JButton downloadButton;

	/**
	 *  Button to get grade of assignment
	 */
  private JButton gradeButton;

	/**
	 *  Button to submit an assignment
	 */
  private JButton dropboxButton;

	/**
	 *  Button to submit an assignment
	 */
  private JButton emailButton;

	/**
	 *  Button to leave GUI
	 */
  private JButton exitButton;

	/**
	 *  The course that the student has accessed
	 */
  private Course myCourse;

	/**
	 * Constructor for the GUI to display 
	 * @param course Course the student has selected
	 * @param assignments Assignments in course
	 */
  public StudentCourseWindow(Course course, Object[] assignments) {
    myCourse = course;

    setTitle(course.name());

    //assignment panel
    JPanel assignmentPanel = new JPanel();
    assignmentPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
    //assignmentPanel.setSize(250, 400);
    assignmentPanel.setLayout(new GridBagLayout());
    GridBagConstraints assignmentConstraints = new GridBagConstraints();
    assignmentConstraints.fill = GridBagConstraints.HORIZONTAL;

    JLabel assignmentLabel = new JLabel("Assignments");
    assignmentConstraints.gridx = 0;
    assignmentConstraints.gridy = 0;
    assignmentConstraints.gridwidth = 4;
    assignmentPanel.add(assignmentLabel, assignmentConstraints);

    downloadButton = new JButton("Download Assignment");
    assignmentConstraints.gridy = 1;
    assignmentConstraints.gridwidth = 4;
    assignmentPanel.add(downloadButton, assignmentConstraints);

    assignmentList = new JList<>(assignmentModel);
    for(int i = 0; i < assignments.length; i++) {
      if(((Assignment)assignments[i]).active())
        assignmentModel.addElement(((Assignment)assignments[i]).ID() + " " + ((Assignment)assignments[i]).title());
    }
    assignmentConstraints.gridy = 2;
    assignmentConstraints.gridwidth = 4;
    assignmentPanel.add(new JScrollPane(assignmentList), assignmentConstraints);

    dropboxButton = new JButton("Assignment Dropbox");
    assignmentConstraints.gridy = 3;
    assignmentConstraints.gridx = 1;
    assignmentConstraints.gridwidth = 2;
    assignmentPanel.add(dropboxButton, assignmentConstraints);
    
    gradeButton = new JButton("Check Grade");
    assignmentConstraints.gridy = 4;
    assignmentPanel.add(gradeButton, assignmentConstraints);

    emailButton = new JButton("E-Mail Professor");
    assignmentConstraints.gridy = 5;
    assignmentPanel.add(emailButton, assignmentConstraints);

    exitButton = new JButton("Back To Course Selection");
    assignmentConstraints.gridy = 6;
    assignmentPanel.add(exitButton, assignmentConstraints);

    add(assignmentPanel);
    pack();
    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    setVisible(true);

  }

	/**
	 * Refreshes the assignments
	 * @param assignments Assignments in course
	 */
  public void updateAssignments(Object[] assignments) {
    assignmentModel.clear();
    for(int i = 0; i < assignments.length; i++) {
      if(((Assignment)assignments[i]).active())
        assignmentModel.addElement(((Assignment)assignments[i]).ID() + " " + ((Assignment)assignments[i]).title());
    }
  }

	/**
	 * Gets the currently selected assignment
	 * @return The String in the assignment list selected
	 */
  public String getSelectedAssignment() {
    return assignmentList.getSelectedValue();
  }

	/**
	 * Gets the currently selected course
	 * @return The course
	 */
  public Course course() { return myCourse; }

	/**
	 * Gets the download button
	 * @return the download button
	 */
  public JButton downloadButton() { return downloadButton; }
  
	/**
	 * Gets the currently selected assignment
	 * @return The String in the assignment list selected
	 */
  public JButton dropboxButton() { return dropboxButton; }
  
	/**
	 * Gets the email button
	 * @return the email button
	 */
  public JButton emailButton() { return emailButton; }
  
	/**
	 * Gets the exit button
	 * @return the exit button
	 */
  public JButton exitButton() { return exitButton; }
  
	/**
	 * Gets the grade button
	 * @return the grade button
	 */
  public JButton gradeButton() { return gradeButton; }

}
