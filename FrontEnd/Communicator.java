package main.FrontEnd;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;

/**
 * A class that takes Serializable objects and sends them over the socket to the server and receives objects.
 * @author Matthew Wiens
 * @author Taylor Huang
 * @author Jeffrey Layton
 */
public class Communicator {
	
	/**
	* The socket connection that will pair to the server.
	*/
	private Socket socket;
	
	/**
	* The writer to send to the server.
	*/
	private ObjectOutputStream socketOut;
	
	/**
	* The reader that receives content from the server.
	*/
	private ObjectInputStream socketIn;

	/**
	 * Basic assignment constructor
	 * @param s Socket to use
	 * @param Out ObjectOutputStream stream to send through the socket
	 * @param In ObjectInputStream stream to receive through the socket
	 */
	public Communicator(Socket s, ObjectOutputStream Out, ObjectInputStream In) {
		socket = s;
		socketIn = In;
		socketOut = Out;
	}
	
	/**
	 * Sends objects through the socket to the server
	 * @param o Object to be sent. Needs to be Serializable.
	 */
	public void sendQuery(Object o) {
		if(o instanceof Serializable) {
			try {
				socketOut.writeObject(o);
				socketOut.flush();
				socketOut.reset();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Receives Objects from the server
	 * @return Object Received
	 */
	public Object getQuery() {
		try {
			return (Object) socketIn.readObject();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
