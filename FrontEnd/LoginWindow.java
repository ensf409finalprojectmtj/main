package main.FrontEnd;

import javax.swing.*;
import java.awt.*;

/**
 * A class that is a Frame for login into the system. Entry Point of the client.
 * @author Matthew Wiens
 * @author Taylor Huang
 * @author Jeffrey Layton
 */
public class LoginWindow extends GUIWindow {

	/**
	 *  Field to enter ID for user
	 */
	private JTextField usernameField;
	
	/**
	 *  Field to enter Password for user
	 */
	private JPasswordField passwordField;
	
	/**
	 *  Button to login
	 */
	private JButton loginButton;

	/**
	 * Constructor for the GUI to display
	 */
	public LoginWindow() {
		setTitle("Login");

		JPanel loginPanel = new JPanel();
		loginPanel.setLayout(new GridBagLayout());
		loginPanel.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
		GridBagConstraints loginConstraints = new GridBagConstraints();
		loginConstraints.fill = GridBagConstraints.HORIZONTAL;

		JLabel usernameLabel = new JLabel("ID Number:");
		loginConstraints.gridx = 0;
		loginConstraints.gridy = 0;
		loginPanel.add(usernameLabel, loginConstraints);
		usernameField = new JTextField(10);
		loginConstraints.gridx = 1;
		loginPanel.add(usernameField, loginConstraints);

		JLabel passwordLabel = new JLabel("Password:");
		loginConstraints.gridx = 0;
		loginConstraints.gridy = 1;
		loginPanel.add(passwordLabel, loginConstraints);
		passwordField = new JPasswordField(10);
		loginConstraints.gridx = 1;
		loginPanel.add(passwordField, loginConstraints);

		loginButton = new JButton("Login");
		loginConstraints.gridx = 1;
		loginConstraints.gridy = 2;
		loginPanel.add(loginButton, loginConstraints);
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		add(loginPanel);
		pack();
		setVisible(true);
	}

	/**
	 * Gets the loginButton
	 */
	public JButton loginButton() { return loginButton; }
	
	/**
	 * Gets the ID textField
	 */
	public String username() { return usernameField.getText(); }
	
	/**
	 * Gets the Password textField
	 */
	public String password() { return new String(passwordField.getPassword()); }

}
