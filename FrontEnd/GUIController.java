package main.FrontEnd;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.Arrays;

import main.data.*;

//by: Matthew Wiens, Taylor Huang, Jeffrey Layton

/**
JAVADOC
*/

/**
 * Acts as the controller in the MVC design;
 * handles client-side events and communicates with Server through Communicator.
 * @author Matthew Wiens
 * @author Taylor Huang
 * @author Jeffrey Layton
 */
public class GUIController {

	/**
	 * Current client GUI to display.
	 */
	private GUIWindow GUI;
	/**
	 * Handles socket communication.
	 */
	private Communicator com;
	/**
	 * Current client user type.
	 */
	private User myUser;
	/**
	 * Current client GUI ActionListener.
	 */
	private myListener listener;

	/**
	 * Subclass that implements ActionListener to handle GUI events, based on the current GUI type
	 */
	private class myListener implements ActionListener {

		/**
		 * Handles ActionEvent based on GUI type.
		 * @param e ActionEvent that has occurred.
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			if(GUI instanceof LoginWindow) {
				if(e.getSource() == ((LoginWindow)GUI).loginButton()) {
					login();
				}
			}
			else if(GUI instanceof ProfessorWindow) {
				if(e.getSource() == ((ProfessorWindow)GUI).selectButton()) {
					inspectCourse();
				}
				else if(e.getSource() == ((ProfessorWindow)GUI).newButton()) {
					createCourse();
				}
			}
			else if(GUI instanceof StudentWindow) {
				if(e.getSource() == ((StudentWindow)GUI).selectButton()) {
					inspectCourseStudent();
				}
			}
			else if(GUI instanceof ProfCourseWindow) {
				if(e.getSource() == ((ProfCourseWindow)GUI).activeBox()) {
					activateCourse();
				}
				if(e.getSource() == ((ProfCourseWindow)GUI).searchButton()) {
					searchStudent();
				}
				else if(e.getSource() == ((ProfCourseWindow)GUI).studentStatusButton()) {
					setStudentStatus();
				}
				else if(e.getSource() == ((ProfCourseWindow)GUI).assignmentStatusButton()) {
					setAssignmentStatus();
				}
				else if(e.getSource() == ((ProfCourseWindow)GUI).addButton()) {
					addAssignment();
				}
				else if(e.getSource() == ((ProfCourseWindow)GUI).exitButton()) {
					exitCourse();
				}
				else if(e.getSource() == ((ProfCourseWindow)GUI).emailButton()) {
					profEmail();
				}
				else if(e.getSource() == ((ProfCourseWindow)GUI).dropboxButton()) {
					profGrade();
				}
			}
			else if(GUI instanceof StudentCourseWindow) {
				if(e.getSource() == ((StudentCourseWindow)GUI).downloadButton()) {
					downloadAssignment();
				}
				else if(e.getSource() == ((StudentCourseWindow)GUI).dropboxButton()) {
					submitAssignment();
				}
				else if(e.getSource() == ((StudentCourseWindow)GUI).emailButton()) {
					studentEmail();
				}
				else if(e.getSource() == ((StudentCourseWindow)GUI).exitButton()) {
					exitCourseStudent();
				}
				else if(e.getSource() == ((StudentCourseWindow)GUI).gradeButton()) {
					studentGrade();
				}
			}
		}

	}

	/**
	 * Sets the protocol for pressing the close button on the GUI to quit the application.
	 */
	public void setExit() {
		GUI.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				JFrame frame = (JFrame) e.getSource();
				int result = JOptionPane.showConfirmDialog(frame, "Exit application?", "Exit", JOptionPane.YES_NO_OPTION);
				if(result == JOptionPane.YES_OPTION)
					exitApp();
			}
		});
	}

	/**
	 * Terminates application by closing the GUI.
	 */
	public void exitApp() {
		GUI.dispose();
	}

	/**
	 * Handles transition to correct GUI after login info is submitted.
	 */
	public void login() {
		String u = ((LoginWindow)GUI).username();
		String p = ((LoginWindow)GUI).password();
		if((u.length() > 0) && (p.length() > 0)) {
			try {
				myUser = new User(Integer.parseInt(u), "", p, "", "", (char) 0, 0);
			} catch(NumberFormatException e) {
				GUI.dispose();
				GUI = new LoginWindow();
				setExit();
				addListener();
				return;
			}
			com.sendQuery(myUser);
			myUser = (User) com.getQuery();
			if(myUser.actionID() == 1) {
				GUI.dispose();
				GUI = new LoginWindow();
				setExit();
				addListener();
			}
			else if(myUser.type() == 'p'){
				myUser.setActionID(1);
				com.sendQuery(myUser);
				Object[] courses = (Object[]) com.getQuery();
				GUI.dispose();
				GUI = new ProfessorWindow(courses);
				setExit();
				addListener();
			}
			else if(myUser.type() == 's'){
				myUser.setActionID(1);
				com.sendQuery(myUser);
				Object[] courses = (Object[]) com.getQuery();
				GUI.dispose();
				GUI = new StudentWindow(courses);
				setExit();
				addListener();
			}
		}
	}

	/**
	 * Allows student to view submitted assignment grades and comments.
	 */
	public void studentGrade() {
		Course course = ((StudentCourseWindow)GUI).course();
		int courseProfID = course.profID();
		course.setProfID(myUser.ID());
		course.setActionID(3);
		com.sendQuery(course);
		course.setProfID(courseProfID);

		Object[] submissions = (Object[]) com.getQuery();
		DefaultListModel<String> submissionModel = new DefaultListModel<>();
	  JList<String> submissionList = new JList<>(submissionModel);
		for(int i = 0; i < submissions.length; i++) {
			String grade;
			if(((Submission)submissions[i]).grade() < 0)
				grade = "Not yet graded";
			else
				grade = Integer.toString(((Submission)submissions[i]).grade());
			submissionModel.addElement(i + " Title: " + ((Submission)submissions[i]).title() + " / Grade: " + grade + " / Comments: " + ((Submission)submissions[i]).comments());
		}

		JPanel submissionPanel = new JPanel();
		submissionPanel.setSize(100, 50);
		submissionPanel.add(new JScrollPane(submissionList));

		int dialogResult = JOptionPane.showOptionDialog(GUI, submissionPanel, "Submission Grades", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
	}

	/**
	 * Allows professor to download or grade a submission.
	 */
	public void profGrade() {
		String[] assignment = ((ProfCourseWindow)GUI).getSelectedAssignment().split(" ");
		Assignment submissionRequest = new Assignment(Integer.parseInt(assignment[0]), ((ProfCourseWindow)GUI).course().ID(), "", "", false, "", 1);
		com.sendQuery(submissionRequest);

		Object[] submissions = (Object[]) com.getQuery();
		DefaultListModel<String> submissionModel = new DefaultListModel<>();
	  JList<String> submissionList = new JList<>(submissionModel);
		for(int i = 0; i < submissions.length; i++) {
			submissionModel.addElement(i + " Student: " + ((Submission)submissions[i]).studentID() + " / Title: " + ((Submission)submissions[i]).title() + " / Grade: " + ((Submission)submissions[i]).grade());
		}

		JPanel assignmentPanel = new JPanel();
		assignmentPanel.setSize(100, 50);
		assignmentPanel.add(new JScrollPane(submissionList));

		int dialogResult = JOptionPane.showOptionDialog(GUI, assignmentPanel, assignment[1], JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, new String[]{"Download", "Grade", "Cancel"}, null);

		if(dialogResult == JOptionPane.YES_OPTION) {
			int submissionNum = Integer.parseInt(submissionList.getSelectedValue().split(" ")[0]);
			Submission assignmentRequest = (Submission) submissions[submissionNum];
			assignmentRequest.setActionID(1);
			com.sendQuery(assignmentRequest);
			assignmentRequest = (Submission) com.getQuery();
			byte[] content = (byte[]) com.getQuery();
			File newFile = new File("profAssignments/");
			newFile.mkdirs();
			newFile = new File("profAssignments/" + assignment[1] + assignmentRequest.fileType());
			try {
				if(!newFile.exists())
					newFile.createNewFile();
				FileOutputStream writer = new FileOutputStream(newFile);
				BufferedOutputStream bos = new BufferedOutputStream(writer);
				bos.write(content);
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		//1
		else if(dialogResult == JOptionPane.NO_OPTION) {
			int submissionNum = Integer.parseInt(submissionList.getSelectedValue().split(" ")[0]);
			Submission graded = (Submission) submissions[submissionNum];

			JTextField gradeField = new JTextField(10);
			JTextField commentField = new JTextField(10);
			Object[] dialog = { "Grade:", gradeField, "Comment:", commentField };
			int result = JOptionPane.showConfirmDialog(GUI, dialog, "Grade Submission", JOptionPane.OK_CANCEL_OPTION);
			if(result == JOptionPane.OK_OPTION) {
				int grade;
				try {
					grade = Integer.parseInt(gradeField.getText());
					if(grade < 0 || grade > 100)
						throw new NumberFormatException();
					graded.setGrade(grade);
					graded.setComments(commentField.getText());
					graded.setActionID(2);
					com.sendQuery(graded);
				}
				catch(NumberFormatException e) {
					JOptionPane.showMessageDialog(GUI, "Invalid grade.");
				}
			}
		}
	}

	/**
	 * Prompts student to send an email to the course professor using the student's stored email address.
	 */
	public void studentEmail() {
		JPasswordField passwordField = new JPasswordField(10);
		JLabel subjectLabel = new JLabel("Subject");
		JTextField subjectField = new JTextField(5);
		JLabel contentLabel = new JLabel("Content");
		JTextArea contentArea = new JTextArea(10, 20);
		JScrollPane contentPane = new JScrollPane(contentArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		JPanel emailPanel = new JPanel();
		emailPanel.setLayout(new GridBagLayout());
		GridBagConstraints emailconstraints = new GridBagConstraints();
		emailconstraints.fill = GridBagConstraints.HORIZONTAL;
		emailconstraints.gridx = 0;
		emailconstraints.gridy = 0;
		emailPanel.add(subjectLabel);
		emailconstraints.gridy = 1;
		emailPanel.add(subjectField);
		emailconstraints.gridy = 2;
		emailPanel.add(contentLabel);
		emailconstraints.gridy = 3;
		emailPanel.add(contentPane);

		Object[] dialog = {"Password:", passwordField, emailPanel };

		int dialogResult = JOptionPane.showConfirmDialog(GUI, dialog, "E-Mail Professor", JOptionPane.OK_CANCEL_OPTION);
		if(dialogResult == JOptionPane.OK_OPTION) {
			//todo: check valid inputs
			String password = new String(passwordField.getPassword());
			Email email = new Email("", null, password, subjectField.getText(), contentArea.getText());
			com.sendQuery(email);
			com.sendQuery(((StudentCourseWindow) GUI).course());
		}
	}

	/**
	 * Prompts professor to send an email to all course students using the professor's stored email address.
	 */
	public void profEmail() {
		JPasswordField passwordField = new JPasswordField(10);
		JLabel subjectLabel = new JLabel("Subject");
		JTextField subjectField = new JTextField(5);
		JLabel contentLabel = new JLabel("Content");
		JTextArea contentArea = new JTextArea(10, 20);
		JScrollPane contentPane = new JScrollPane(contentArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		JPanel emailPanel = new JPanel();
		emailPanel.setLayout(new GridBagLayout());
		GridBagConstraints emailconstraints = new GridBagConstraints();
		emailconstraints.fill = GridBagConstraints.HORIZONTAL;
		emailconstraints.gridx = 0;
		emailconstraints.gridy = 0;
		emailPanel.add(subjectLabel);
		emailconstraints.gridy = 1;
		emailPanel.add(subjectField);
		emailconstraints.gridy = 2;
		emailPanel.add(contentLabel);
		emailconstraints.gridy = 3;
		emailPanel.add(contentPane);

		Object[] dialog = {"Password:", passwordField, emailPanel };

		int dialogResult = JOptionPane.showConfirmDialog(GUI, dialog, "E-Mail All Students", JOptionPane.OK_CANCEL_OPTION);
		if(dialogResult == JOptionPane.OK_OPTION) {
			//todo: check valid inputs
			String password = new String(passwordField.getPassword());
			Email email = new Email("", null, password, subjectField.getText(), contentArea.getText());
			com.sendQuery(email);
			com.sendQuery(((ProfCourseWindow) GUI).course());
		}
	}

	/**
	 * Downloads a selected assignment to the student's computer.
	 */
	public void downloadAssignment() {
		String[] assignment = ((StudentCourseWindow)GUI).getSelectedAssignment().split(" ");
		Assignment assignmentRequest = new Assignment(Integer.parseInt(assignment[0]), ((StudentCourseWindow)GUI).course().ID(), "", "", false, "", 4);
		com.sendQuery(assignmentRequest);
		byte[] content = null;
		assignmentRequest = (Assignment) com.getQuery();
		content = (byte[]) com.getQuery();
		File newFile = new File("studentAssignments/");
		newFile.mkdirs();
		assignment = Arrays.copyOfRange(assignment, 1, assignment.length);
		String name = String.join(" ", assignment);
		newFile = new File("studentAssignments/" + name + assignmentRequest.fileType());
		try {
			if(!newFile.exists())
				newFile.createNewFile();
			FileOutputStream writer = new FileOutputStream(newFile);
			BufferedOutputStream bos = new BufferedOutputStream(writer);
			bos.write(content);
			bos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Submits a selected document from the student's computer.
	 */
	public void submitAssignment() {
		String[] assignment = ((StudentCourseWindow)GUI).getSelectedAssignment().split(" ");

		JFileChooser fileChooser = new JFileChooser();
		int choice = fileChooser.showOpenDialog(GUI);
		if (choice == JFileChooser.APPROVE_OPTION) {
    	File file = fileChooser.getSelectedFile();
			long length = file.length();
			byte[] content = new byte[(int) length];
			try {
				BufferedInputStream b = new BufferedInputStream(new FileInputStream(file));
				b.read(content, 0, (int) length);
			}
			catch(FileNotFoundException e) {
				e.printStackTrace();
			}
			catch(IOException e) {
				e.printStackTrace();
			}
			int temp = Integer.parseInt(assignment[0]);
			assignment = Arrays.copyOfRange(assignment, 1, assignment.length);
			Submission submission = new Submission(1, temp, myUser.ID(), String.join(" ", assignment), "", 0, "", "", 0);
			String s = file.getPath();
			s = s.substring(s.length()-4);
			submission.setFileType(s);
			com.sendQuery(submission);
			com.sendQuery(content);
		}
	}

	/**
	 * Allows the professor to search for students by ID or last name.
	 */
	public void searchStudent() {
		String search = ((ProfCourseWindow)GUI).search();
		if(search.length() > 0) {
			for(int i = 0; i < search.length(); i++) {
				if((search.charAt(i) < '0') || (search.charAt(i) > '9')){
					// 2
					com.sendQuery(new User(0, "", "", "", search, (char) 0, 2));
					Object[] students = (Object[]) com.getQuery();
					((ProfCourseWindow)GUI).updateStudents(students);
					com.sendQuery(new Course(((ProfCourseWindow)GUI).course().ID(), 0, "", false, 2));
					Object[] assignments = (Object[]) com.getQuery();
					((ProfCourseWindow)GUI).updateAssignments(assignments);
					GUI.dispose();
					GUI = new ProfCourseWindow(((ProfCourseWindow)GUI).course(), assignments, students);
					setExit();
					addListener();
					return;
				}
			}
			// 3
			com.sendQuery(new User(Integer.parseInt(search), "", "", "", "", (char) 0, 3));
			Object[] students = (Object[]) com.getQuery();
			((ProfCourseWindow)GUI).updateStudents(students);
			com.sendQuery(new Course(((ProfCourseWindow)GUI).course().ID(), 0, "", false, 2));
			Object[] assignments = (Object[]) com.getQuery();
			((ProfCourseWindow)GUI).updateAssignments(assignments);
			GUI.dispose();
			GUI = new ProfCourseWindow(((ProfCourseWindow)GUI).course(), assignments, students);
			setExit();
			addListener();
		}
		else {
			updateLists();
		}
	}

	/**
	 * Allows professor to create a new course.
	 */
	public void createCourse() {
		JTextField nameField = new JTextField(10);

		Object[] dialog = { "Course Name:", nameField };
		int dialogResult = JOptionPane.showConfirmDialog(GUI, dialog, "Enter course details", JOptionPane.OK_CANCEL_OPTION);
		if(dialogResult == JOptionPane.OK_OPTION) {
			//todo: check valid inputs
			Course course = new Course(1, myUser.ID(), nameField.getText(), false, 4);
			com.sendQuery(course);
			myUser.setActionID(1);
			com.sendQuery(myUser);
			Object[] courses = (Object[]) com.getQuery();
			GUI.dispose();
			GUI = new ProfessorWindow(courses);
			setExit();
			addListener();
		}
	}

	/**
	 * Transitions the professor course menu GUI into the single course GUI.
	 */
	public void inspectCourse() {
		Course inspect = ((ProfessorWindow)GUI).courseSelected();
		inspect.setActionID(2);
		com.sendQuery(inspect);
		Object[] assignments = (Object[]) com.getQuery();
		inspect.setActionID(1);
		com.sendQuery(inspect);
		Object[] students = (Object[]) com.getQuery();
		GUI.dispose();
		GUI = new ProfCourseWindow(inspect, assignments, students);
		setExit();
		addListener();
	}

	/**
	 * Transitions the student course menu GUI into the single course GUI.
	 */
	public void inspectCourseStudent() {
		Course inspect = ((StudentWindow)GUI).courseSelected();
		inspect.setActionID(2);
		com.sendQuery(inspect);
		Object[] assignments = (Object[]) com.getQuery();
		GUI.dispose();
		GUI = new StudentCourseWindow(inspect, assignments);
		setExit();
		addListener();
	}

	/**
	 * Allows the professor to activate or deactivate a course.
	 */
	public void activateCourse() {
		if(((ProfCourseWindow)GUI).activeBox().isSelected()) {
			((ProfCourseWindow)GUI).course().setActive(true);
		}
		else {
			((ProfCourseWindow)GUI).course().setActive(false);
		}
		((ProfCourseWindow)GUI).course().setActionID(0);
		com.sendQuery(((ProfCourseWindow)GUI).course());
	}

	/**
	 * Allows professor to return to course menu GUI.
	 */
	public void exitCourse() {
		myUser.setActionID(1);
		com.sendQuery(myUser);
		Object[] courses = (Object[]) com.getQuery();
		GUI.dispose();
		GUI = new ProfessorWindow(courses);
		setExit();
		addListener();
	}

	/**
	 * Allows student to return to course menu GUI.
	 */
	public void exitCourseStudent() {
		myUser.setActionID(1);
		com.sendQuery(myUser);
		Object[] courses = (Object[]) com.getQuery();
		GUI.dispose();
		GUI = new StudentWindow(courses);
		setExit();
		addListener();
	}

	/**
	 * Changes the selected student's enrollment status.
	 */
	public void setStudentStatus() {
		String student = ((ProfCourseWindow)GUI).getSelectedStudent();
		String[] studentID = student.split(" ");
		if(student.charAt(student.length()-1) == '1') //1
			com.sendQuery(new Enrollment(0, ((ProfCourseWindow)GUI).course().ID(), Integer.parseInt(studentID[0]), 1));
		else //0
			com.sendQuery(new Enrollment(0, ((ProfCourseWindow)GUI).course().ID(), Integer.parseInt(studentID[0]), 0));
		updateLists();
	}

	/**
	 * Changes the selected assignment's active status.
	 */
	public void setAssignmentStatus() {
		String assignment = ((ProfCourseWindow)GUI).getSelectedAssignment();
		String[] assignmentID = assignment.split(" ");
		if(assignment.charAt(assignment.length()-1) == '1')
			com.sendQuery(new Assignment(Integer.parseInt(assignmentID[0]), 0, "", "", false, "", 0));
		else
			com.sendQuery(new Assignment(Integer.parseInt(assignmentID[0]), 0, "", "", true, "", 0));
		updateLists();
	}

	/**
	 * Updates the professor's course GUI lists.
	 */
	public void updateLists() {
		com.sendQuery(new Course(((ProfCourseWindow)GUI).course().ID(), 0, "", false, 2));
		Object[] assignments = (Object[]) com.getQuery();
		((ProfCourseWindow)GUI).updateAssignments(assignments);
		com.sendQuery(new Course(((ProfCourseWindow)GUI).course().ID(), 0, "", false, 1));
		Object[] students = (Object[]) com.getQuery();
		((ProfCourseWindow)GUI).updateStudents(students);
		GUI.dispose();
		GUI = new ProfCourseWindow(((ProfCourseWindow)GUI).course(), assignments, students);
		setExit();
		addListener();
	}

	/**
	 * Updates the student's course GUI lists.
	 */
	public void updateListsStudent() {
		com.sendQuery(new Course(((StudentCourseWindow)GUI).course().ID(), 0, "", false, 2));
		Object[] assignments = (Object[]) com.getQuery();
		((StudentCourseWindow)GUI).updateAssignments(assignments);
		GUI.dispose();
		GUI = new StudentCourseWindow(((StudentCourseWindow)GUI).course(), assignments);
		setExit();
		addListener();
	}

	/**
	 * Adds a selected assignment from the professor's computer.
	 */
	public void addAssignment() {
		JFileChooser fileChooser = new JFileChooser();
		int choice = fileChooser.showOpenDialog(GUI);
		if (choice == JFileChooser.APPROVE_OPTION) {
    	File file = fileChooser.getSelectedFile();
			long length = file.length();
			byte[] content = new byte[(int) length];
			try {
				BufferedInputStream b = new BufferedInputStream(new FileInputStream(file));
				b.read(content, 0, (int) length);
			}
			catch(FileNotFoundException e) {
				e.printStackTrace();
			}
			catch(IOException e) {
				e.printStackTrace();
			}

			JTextField titleField = new JTextField(10);
			JTextField dueDateField = new JTextField(10);

			Object[] dialog = { "Title:", titleField,
											    "Due Date:", dueDateField };
			int dialogResult = JOptionPane.showConfirmDialog(GUI, dialog,
			                                                 "Enter assignment details",
																											 JOptionPane.OK_CANCEL_OPTION);
			if(dialogResult == JOptionPane.OK_OPTION) {
				//todo: check valid inputs
				Assignment assignment = new Assignment(1, ((ProfCourseWindow)GUI).course().ID(), titleField.getText(), "",
																		           false, dueDateField.getText(), 3);
				String s = file.getPath();
				s = s.substring(s.length()-4);
				assignment.setFileType(s);
				com.sendQuery(assignment);
				com.sendQuery(content);
				updateLists();
			}
		}
	}

	/**
	 * Adds ActionListener monitoring to JComponents based on the current GUI.
	 */
	public void addListener() {
		if(GUI instanceof LoginWindow) {
			((LoginWindow)GUI).loginButton().addActionListener(listener);
		}
		else if(GUI instanceof ProfessorWindow) {
			((ProfessorWindow)GUI).selectButton().addActionListener(listener);
			((ProfessorWindow)GUI).newButton().addActionListener(listener);
		}
		else if(GUI instanceof StudentWindow) {
			((StudentWindow)GUI).selectButton().addActionListener(listener);
		}
		else if(GUI instanceof ProfCourseWindow) {
			((ProfCourseWindow)GUI).activeBox().addActionListener(listener);
			((ProfCourseWindow)GUI).searchButton().addActionListener(listener);
			((ProfCourseWindow)GUI).studentStatusButton().addActionListener(listener);
			((ProfCourseWindow)GUI).assignmentStatusButton().addActionListener(listener);
			((ProfCourseWindow)GUI).addButton().addActionListener(listener);
			((ProfCourseWindow)GUI).exitButton().addActionListener(listener);
			((ProfCourseWindow)GUI).emailButton().addActionListener(listener);
			((ProfCourseWindow)GUI).dropboxButton().addActionListener(listener);
		}
		else if(GUI instanceof StudentCourseWindow) {
			((StudentCourseWindow)GUI).downloadButton().addActionListener(listener);
			((StudentCourseWindow)GUI).emailButton().addActionListener(listener);
			((StudentCourseWindow)GUI).exitButton().addActionListener(listener);
			((StudentCourseWindow)GUI).dropboxButton().addActionListener(listener);
			((StudentCourseWindow)GUI).gradeButton().addActionListener(listener);
		}
	}

	/**
	 * Constructs a GUIController with a specified Communicator and a new ActionListener.
	 */
	public GUIController(Communicator c) {
		com = c;
		listener = new myListener();
	}

	/**
	 * Sets current GUI to specified GUI.
	 */
	public void change_GUI(GUIWindow G) {
		GUI = G;
	}

}
