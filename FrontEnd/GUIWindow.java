package main.FrontEnd;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/**
 * An abstract class that is used to extend GUIs
 * @author Matthew Wiens
 * @author Taylor Huang
 * @author Jeffrey Layton
 */
public abstract class GUIWindow extends JFrame {


	/**
	* Default Constructor
	*/
	public GUIWindow() { 
	}

}
