package main.FrontEnd;

import main.data.*;
import javax.swing.*;
import java.awt.*;

//by: Matthew Wiens, Taylor Huang, Jeffrey Layton

/**
 * The professor course options GUI.
 * @author Matthew Wiens
 * @author Taylor Huang
 * @author Jeffrey Layton
 */
public class ProfCourseWindow extends GUIWindow {

  /**
   * Check box to set course activation status.
   */
  private JCheckBox activeBox;

  /**
   * Student search field.
   */
  private JTextField searchField;
  /**
   * Student search button.
   */
  private JButton searchButton;

  /**
   * Model to contain the list of students, allowing only one to be selected at a time.
   */
  private DefaultListModel<String> studentModel = new DefaultListModel<>();
  /**
   * List that utilizes the DefaultListModel to contain the list of students.
   */
  private JList<String> studentList;

  /**
   * Button to change selected student status.
   */
  private JButton studentStatusButton;
  /**
   * Button to send an email to all students.
   */
  private JButton emailButton;

  /**
   * Model to contain the list of assignments, allowing only one to be selected at a time.
   */
  private DefaultListModel<String> assignmentModel = new DefaultListModel<>();
  /**
   * List that utilizes the DefaultListModel to contain the list of assignments.
   */
  private JList<String> assignmentList;

  /**
   * Button to access submission dropbox for an assignment.
   */
  private JButton dropboxButton;
  /**
   * Button to change the actvie status of an assignment.
   */
  private JButton assignmentStatusButton;
  /**
   * Button to add an assignment.
   */
  private JButton addButton;

  /**
   * Button to return to course selection.
   */
  private JButton exitButton;

  /**
   * Current course.
   */
  private Course myCourse;

  /**
	 * Creates the GUI and enables it.
	 */
  public ProfCourseWindow(Course course, Object[] assignments, Object[] students) {
    myCourse = course;

    setTitle(course.name());
    //setSize(500, 500);

    JPanel coursePanel = new JPanel();
    //coursePanel.setSize(500, 500);
    coursePanel.setLayout(new GridBagLayout());
    coursePanel.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
    GridBagConstraints courseConstraints = new GridBagConstraints();

    JLabel activeLabel = new JLabel("Active Course");
    courseConstraints.gridx = 0;
		courseConstraints.gridy = 0;
    courseConstraints.gridwidth = 1;
    courseConstraints.fill = GridBagConstraints.EAST;
    coursePanel.add(activeLabel, courseConstraints);
    activeBox = new JCheckBox();
    if(course.active())
      activeBox.setSelected(true);
    else
      activeBox.setSelected(false);
    courseConstraints.fill = GridBagConstraints.HORIZONTAL;
    courseConstraints.gridx = 1;
    coursePanel.add(activeBox, courseConstraints);

    //student panel
    JPanel studentPanel = new JPanel();
    studentPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
    //studentPanel.setSize(250, 400);
    studentPanel.setLayout(new GridBagLayout());
    GridBagConstraints studentConstraints = new GridBagConstraints();
    studentConstraints.fill = GridBagConstraints.HORIZONTAL;

    JLabel studentLabel = new JLabel("Students");
    studentConstraints.gridx = 0;
    studentConstraints.gridy = 0;
    studentConstraints.gridwidth = 3;
    studentPanel.add(studentLabel, studentConstraints);

    searchField = new JTextField(10);
    studentConstraints.gridx = 0;
    studentConstraints.gridy = 1;
    studentConstraints.gridwidth = 1;
    studentPanel.add(searchField, studentConstraints);
    searchButton = new JButton("Search");
    studentConstraints.gridx = 1;
    studentPanel.add(searchButton, studentConstraints);

    studentList = new JList<>(studentModel);
    for(int i = 0; i < students.length; i++) {
      if(((User)students[i]).actionID() == 1)
        studentModel.addElement(((User)students[i]).ID() + " " + ((User)students[i]).firstName() + " " + ((User)students[i]).lastName() + " Enrolled 1");
      else
        studentModel.addElement(((User)students[i]).ID() + " " + ((User)students[i]).firstName() + " " + ((User)students[i]).lastName() + " Not Enrolled 0");
    }
    studentConstraints.gridy = 2;
    studentConstraints.gridx = 0;
    studentConstraints.gridwidth = 3;
    studentPanel.add(new JScrollPane(studentList), studentConstraints);

    studentStatusButton = new JButton("Change Enrollment Status");
    studentConstraints.gridy = 3;
    studentConstraints.gridx = 0;
    studentPanel.add(studentStatusButton, studentConstraints);

    emailButton = new JButton("E-Mail Students");
    studentConstraints.gridy = 4;
    studentPanel.add(emailButton, studentConstraints);

    courseConstraints.gridy = 1;
    courseConstraints.gridx = 0;
    coursePanel.add(studentPanel, courseConstraints);
    //student panel

    //assignment panel
    JPanel assignmentPanel = new JPanel();
    assignmentPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
    //assignmentPanel.setSize(250, 400);
    assignmentPanel.setLayout(new GridBagLayout());
    GridBagConstraints assignmentConstraints = new GridBagConstraints();
    assignmentConstraints.fill = GridBagConstraints.HORIZONTAL;

    JLabel assignmentLabel = new JLabel("Assignments (0 for Inactive, 1 for Active)");
    assignmentConstraints.gridx = 0;
    assignmentConstraints.gridy = 0;
    assignmentConstraints.gridwidth = 4;
    assignmentPanel.add(assignmentLabel, assignmentConstraints);

    assignmentList = new JList<>(assignmentModel);
    for(int i = 0; i < assignments.length; i++) {
      if(((Assignment)assignments[i]).active())
        assignmentModel.addElement(((Assignment)assignments[i]).ID() + " " + ((Assignment)assignments[i]).title() + " 1");
      else
        assignmentModel.addElement(((Assignment)assignments[i]).ID() + " " + ((Assignment)assignments[i]).title() + " 0");
    }
    assignmentConstraints.gridy = 1;
    assignmentPanel.add(new JScrollPane(assignmentList), assignmentConstraints);

    dropboxButton = new JButton("View Dropbox");
    assignmentConstraints.gridy = 2;
    assignmentConstraints.gridx = 0;
    assignmentConstraints.gridwidth = 2;
    assignmentPanel.add(dropboxButton, assignmentConstraints);
    assignmentStatusButton = new JButton("Change Assignment Status");
    assignmentConstraints.gridx = 2;
    assignmentPanel.add(assignmentStatusButton, assignmentConstraints);

    addButton = new JButton("Add Assignment");
    assignmentConstraints.gridy = 3;
    assignmentConstraints.gridx = 0;
    assignmentConstraints.gridwidth = 4;
    assignmentPanel.add(addButton, assignmentConstraints);

    exitButton = new JButton("Back To Course Selection");
    assignmentConstraints.gridy = 4;
    assignmentPanel.add(exitButton, assignmentConstraints);

    courseConstraints.gridx = 1;
    coursePanel.add(assignmentPanel, courseConstraints);
    //assignment panel

    add(coursePanel);
    pack();
    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    setVisible(true);
  }

  /**
   * Updates the assignment list.
   */
  public void updateAssignments(Object[] assignments) {
    assignmentModel.clear();
    for(int i = 0; i < assignments.length; i++) {
      if(((Assignment)assignments[i]).active())
        assignmentModel.addElement(((Assignment)assignments[i]).ID() + " " + ((Assignment)assignments[i]).title() + " 1");
      else
        assignmentModel.addElement(((Assignment)assignments[i]).ID() + " " + ((Assignment)assignments[i]).title() + " 0");
    }
  }

  /**
   * Updates the student list.
   */
  public void updateStudents(Object[] students) {
	  studentModel.clear();
	  for(int i = 0; i < students.length; i++) {
	      if(((User)students[i]).actionID() == 1)
	        studentModel.addElement(((User)students[i]).ID() + " " + ((User)students[i]).firstName() + " " + ((User)students[i]).lastName() + " Enrolled 1");
	      else
	    	studentModel.addElement(((User)students[i]).ID() + " " + ((User)students[i]).firstName() + " " + ((User)students[i]).lastName() + " Not Enrolled 0");
	  }
  }

  /**
   * Returns current course.
   * @return myCourse
   */
  public Course course() { return myCourse; }

  /**
   * Returns selected student from list.
   * @return String
   */
  public String getSelectedStudent() {
    return studentList.getSelectedValue();
  }
  /**
   * Returns selected assignment from list.
   * @return String
   */
  public String getSelectedAssignment() {
    return assignmentList.getSelectedValue();
  }

  /**
   * Returns checkbox status.
   * @return activeBox
   */
  public JCheckBox activeBox() { return activeBox; }
  /**
   * Returns search button.
   * @return searchButton
   */
  public JButton searchButton() { return searchButton; }
  /**
   * Returns search field text.
   * @return String
   */
  public String search() {
    return searchField.getText();
  }
  /**
   * Returns student status change button.
   * @return studentStatusButton
   */
  public JButton studentStatusButton() { return studentStatusButton; }
  /**
   * Returns assignment status change button.
   * @return assignmentStatusButton
   */
  public JButton assignmentStatusButton() { return assignmentStatusButton; }
  /**
   * Returns add course button.
   * @return addButton
   */
  public JButton addButton() { return addButton; }
  /**
   * Returns exit course button.
   * @return exitButton
   */
  public JButton exitButton() { return exitButton; }
  /**
   * Returns email students button.
   * @return emailButton
   */
  public JButton emailButton() { return emailButton; }
  /**
   * Returns dropbox button.
   * @return dropboxButton
   */
  public JButton dropboxButton() { return dropboxButton; }


}
