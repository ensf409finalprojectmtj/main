package main.FrontEnd;

//by: Matthew Wiens, Taylor Huang, Jeffrey Layton
import main.data.Course;

import javax.swing.*;
import java.awt.*;

/**
 * The student user course selection GUI.
 * @author Matthew Wiens
 * @author Taylor Huang
 * @author Jeffrey Layton
 */
public class StudentWindow extends GUIWindow {

	/**
	 * Combo box containing all courses currently enrolled in.
	 */
	private JComboBox<String> coursesBox;
	/**
	 * Button to confirm course selection.
	 */
	private JButton selectButton;
	/**
	 * Array containing all enrolled courses.
	 */
	private Object[] courses;

	/**
	 * Creates the GUI and enables it.
	 */
	public StudentWindow(Object[] c) {
		courses = c;
		String[] courseNames = new String[courses.length];
		for(int i = 0; i < courses.length; i++) {
			courseNames[i] = ((Course)courses[i]).name();
		}

		setTitle("Select a course");

		JPanel studentPanel = new JPanel();
		studentPanel.setLayout(new GridBagLayout());
		studentPanel.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
		//setSize(200, 100);

		GridBagConstraints studentConstraints = new GridBagConstraints();
		studentConstraints.fill = GridBagConstraints.HORIZONTAL;

		JLabel coursesLabel = new JLabel("Courses:");
		studentConstraints.gridx = 0;
		studentConstraints.gridy = 0;
		studentPanel.add(coursesLabel, studentConstraints);

		coursesBox = new JComboBox<String>(courseNames);
		studentConstraints.gridx = 1;
		studentPanel.add(coursesBox, studentConstraints);

		selectButton = new JButton("Select");
		studentConstraints.gridx = 1;
		studentConstraints.gridy = 1;
		studentConstraints.gridwidth = 2;
		studentPanel.add(selectButton, studentConstraints);

		add(studentPanel);
		pack();
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setVisible(true);
	}

	/**
	 * Returns select button.
	 * @return selectButton.
	 */
	public JButton selectButton() { return selectButton; }

	/**
	 * Returns selected course.
	 * @return Course
	 */
	public Course courseSelected() {
		String course = (String) coursesBox.getSelectedItem();
		for(int i = 0; i < courses.length; i++) {
			if(((Course)courses[i]).name() == course){
				return (Course) courses[i];
			}
		}
		return null;
	}

}
