package main.FrontEnd;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import main.BackEnd.Server;

/**
 * A class that holds the main for the client side of the program.
 * @author Matthew Wiens
 * @author Taylor Huang
 * @author Jeffrey Layton
 */
public class Client {

	
	/**
	 * Run the Client side.
	 * 
	 * @param args {Socket name,  
	 */
	public static void main(String[] args) {
		if(args.length != 2) {//MUST be 2 inputs to work
			System.err.println("Invalid number of arguments.");//Error message
			System.exit(1);//Abort
		}
		
		Socket palinSocket;
		ObjectOutputStream socketOut;
		ObjectInputStream socketIn;
		Communicator c;
		GUIController GUIcon;
		try {
			palinSocket = new Socket(args[0], Integer.parseInt(args[1]));
			socketOut = new ObjectOutputStream(palinSocket.getOutputStream());
			socketIn = new ObjectInputStream(palinSocket.getInputStream());
			c = new Communicator(palinSocket, socketOut, socketIn);
			GUIcon = new GUIController(c);
			LoginWindow start = new LoginWindow();
			GUIcon.change_GUI(start);
			GUIcon.setExit();
			GUIcon.addListener();
		}catch(NumberFormatException e) {
			System.err.println("Not a integer");//Error message
			System.exit(1);//Abort
		} catch (IOException e) {
			System.err.println("Error in Connecting...Abort...");
			System.exit(1);
		}
	}
}
