package main.FrontEnd;

import main.data.Course;

import javax.swing.*;
import java.awt.*;

//by: Matthew Wiens, Taylor Huang, Jeffrey Layton

/**
 * The professor user course selection GUI.
 * @author Matthew Wiens
 * @author Taylor Huang
 * @author Jeffrey Layton
 */
public class ProfessorWindow extends GUIWindow {
	/**
	 * Combo box containing all courses currently enrolled in.
	 */
	private JComboBox<String> coursesBox;
	/**
	 * Button to confirm course selection.
	 */
	private JButton selectButton;
	/**
	 * Button to create new course.
	 */
	private JButton newButton;
	/**
	 * Array containing all courses.
	 */
	private Object[] courses;

	/**
	 * Creates the GUI and enables it.
	 */
	public ProfessorWindow(Object[] c) {
		courses = c;
		String[] courseNames = new String[courses.length];
		for(int i = 0; i < courses.length; i++) {
			courseNames[i] = ((Course)courses[i]).name();
		}

		setTitle("Select a course");

		JPanel profPanel = new JPanel();
		profPanel.setLayout(new GridBagLayout());
		profPanel.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
		//setSize(200, 100);

		GridBagConstraints profConstraints = new GridBagConstraints();
		profConstraints.fill = GridBagConstraints.HORIZONTAL;

		JLabel coursesLabel = new JLabel("Courses:");
		profConstraints.gridx = 0;
		profConstraints.gridy = 0;
		profPanel.add(coursesLabel, profConstraints);

		coursesBox = new JComboBox<String>(courseNames);
		profConstraints.gridx = 1;
		profPanel.add(coursesBox, profConstraints);

		selectButton = new JButton("Select");
		profConstraints.gridx = 1;
		profConstraints.gridy = 1;
		profPanel.add(selectButton, profConstraints);

		newButton = new JButton("Create Course");
		profConstraints.gridy = 2;
		profPanel.add(newButton, profConstraints);

		add(profPanel);
		pack();
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setVisible(true);
	}

	/**
	 * Returns select button.
	 * @return selectButton.
	 */
	public JButton selectButton() { return selectButton; }

	/**
	 * Returns new course button.
	 * @return newButton.
	 */
	public JButton newButton() { return newButton; }

	/**
	 * Returns selected course.
	 * @return Course
	 */
	public Course courseSelected() {
		String course = (String) coursesBox.getSelectedItem();
		for(int i = 0; i < courses.length; i++) {
			if(((Course)courses[i]).name() == course){
				return (Course) courses[i];
			}
		}
		return null;
	}

}
