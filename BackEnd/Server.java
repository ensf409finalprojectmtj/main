package main.BackEnd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * server class performs the class of instantiating the server, 
 * setting up a socket and adding clients to threads as they
 * request to join the server. 
 * @author Matthew Wiens 
 * @author Taylor Huang
 * @author Jeffrey Layton
 *
 */

public class Server {


	/**
	 * Provides a runnable class to monitor the command line inputs for a server for shut down
	* @author Jeffrey Layton
	* @version 1.0
	* @since March 13th, 2018
	 */
	public static class Commands implements Runnable {

		/**
		 * Connection to a server
		 */
		private Server server;

		/**
		 * Command line input
		 */
		private BufferedReader stdIn;

		/**
		 * Constructor for runnable
		 * @param std Command line input
		 * @param ser server connection input
		 */
		public Commands(BufferedReader std, Server ser) {
			stdIn = std;
			server = ser;
		}

		/**
		 * Runnable for commands. If QUIT is inputed, shut server down.
		 */
		public void run() {
			String line = "";
			while(true) {
				try {
					if(stdIn.ready())
						line = stdIn.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
				if(line.equalsIgnoreCase("QUIT")) {
					server.shutdown();
					try {
						stdIn.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					System.exit(0);
				}
				try {
					Thread.sleep(25);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * Handles thread execution and timing.
	 */
	private ExecutorService executor;

	/**
	 * Server-side socket connection.
	 */
	private ServerSocket ssocket;

	/**
	 * Manages database communication.
	 */
	private DataBaseManager data;

	/**
	 * Initialize the serverSocket connection and starts a thread pool to 15 threads for multiple games to run.
	 * @param x the port number of the server.
	 * @param connection connection info to the SQL database
	 * @param login username for the SQL database user
	 * @param password password for the SQL database user
	 */
	public Server(int x, String connection, String login, String password){
		try {
			ssocket = new ServerSocket(x);
			System.out.println("Server is now running....");
			executor = Executors.newFixedThreadPool(15);
			data = new DataBaseManager(connection,login,password);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Responsible for shutting down the executer, serverSockets and temporary connections.
	 */
	public void shutdown() {
    	System.out.println("Server Closing...");
        try {
        	executor.shutdownNow();
            if(ssocket != null)
            	ssocket.close();
		} catch (SocketException e) {
			System.err.println("Closing error: " + e.getMessage());
		} catch (IOException e) {
			System.err.println("Closing error: " + e.getMessage());
		}
	}

	/**
	 * Accepts connections and executes threads for multiple client support.
	 */
	public void manageThreads() {
		Socket s = null;
		ObjectOutputStream socketOutput = null;
		ObjectInputStream socketInput = null;
		Communicator c = null;
		while(true) {
			try {
				s = ssocket.accept();
				socketOutput = new ObjectOutputStream(s.getOutputStream());
				socketInput = new ObjectInputStream(s.getInputStream());
			} catch (SocketException e) {
				//System.err.println("Closing error: " + e.getMessage());
			} catch(IOException e) {
				e.printStackTrace();// TODO SHOULD HANDLE
			}
			c = new Communicator(s,socketOutput,socketInput);
			executor.execute(new Controller(data,c));
		}
	}

	/**
	 * Initializes everything and starts server.
	 * @param args { port number, connection info to the SQL database, usename for SQL database, password for SQL database }
	 */
	public static void main(String[] args) {

		if(args.length != 4) {//MUST be 4 inputs to work
			System.err.println("Invalid number of arguments.");//Error message
			System.exit(1);//Abort
		}

		Server s = null;
		try {
			s = new Server(Integer.parseInt(args[0]), args[1], args[2], args[3]);
		}catch(NumberFormatException e) {
			System.err.println("Not a integer");//Error message
			System.exit(1);//Abort
		}catch(Exception e) {
			System.err.println("Error setting up server");//Error message
			System.exit(1);//Abort
		}
		Commands com = new Commands(new BufferedReader(new InputStreamReader(System.in)),s);
		(new Thread(com)).start();
		s.manageThreads();
	}

}
