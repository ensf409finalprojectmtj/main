package main.BackEnd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * A class manages IO of the database to the threads in the server.
 * @author Matthew Wiens
 * @author Taylor Huang
 * @author Jeffrey Layton
 */
public class DataBaseManager {
	
	/**
	 *  Connection to the server
	 */
	private Connection jdbc_connection;
	
	/**
	 *  String of the connection info
	 */
	private String conInfo;
	
	/**
	 *  String for the login to the server
	 */
	private String login;
	
	/**
	 *  String for the password to the server
	 */
	private String password;
	
	/**
	 *  Temporary prepared statement
	 */
	public PreparedStatement statement;
	
	
	/**
	 * Constructor to connect to the data base
	 * @param connection Connection info
	 * @param log login name
	 * @param pass password for login
	 */
	public DataBaseManager(String connection, String log, String pass) { // taken from assignment 8
		conInfo = connection;
		login = log;
		password = pass;
		try{
			// If this throws an error, make sure you have added the mySQL connector JAR to the project
			Class.forName("com.mysql.jdbc.Driver");
			
			// If this fails make sure your connectionInfo and login/password are correct
			jdbc_connection = DriverManager.getConnection(connection, log, pass);
			System.out.println("Connected to: " + connection + "\n");
		}
		catch(SQLException e) { e.printStackTrace(); }
		catch(Exception e) { e.printStackTrace(); }
	}
	
	/**
	 * Send a query to the data base and receive the results
	 * @param sql String for the command
	 * @return Return of the set
	 */
	public synchronized ResultSet query(String sql) {
		ResultSet customer = null;
		try{
			statement = jdbc_connection.prepareStatement(sql);
			customer = statement.executeQuery();
		}
		catch(SQLException e)
		{
			System.out.println("SQL Error");
			e.printStackTrace();
		}
		return customer;
	}
	
	/**
	 * Send a statement to the data base
	 * @param sql String for the command
	 */
	public synchronized void send(String sql) {
		try{
			statement = jdbc_connection.prepareStatement(sql);
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			System.out.println("SQL Error");
			e.printStackTrace();
		}
	}
}
