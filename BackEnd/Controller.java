package main.BackEnd;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;

import main.data.*;

/**
 * controller class deals with all communications with the
 * client and sends requests to the sql database. It implements
 * an army of methods to deal with all of the possible capabilities
 * of the system. 
 * @author Matthew Wiens
 * @author Jeffrey Layton
 * @author Taylor Huang
 *
 */

public class Controller implements Runnable {
	/**
	 * pointer to the communicator which deals with
	 * all communications to the socket. 
	 */
	private Communicator com;
	/**
	 * pointer to database which deals with all comms
	 * to the sql server. 
	 */
	private DataBaseManager database;
	/**
	 * local holder for user email. 
	 */
	private String email;
	/**
	 * type designates student or teacher. 
	 */
	private char type;

	/**
	 * default constructor initalizes all commmunication components. 
	 * @param data pointer to database
	 * @param c pointer to communicator
	 */
	public Controller(DataBaseManager data, Communicator c) {
		database = data;
		com = c;
		type = 'u';
	}

	/**
	 * checks database for necessary tables and builds them if
	 * they do not exist. 
	 */
	public void checkTables() {
		database.send("CREATE TABLE IF NOT EXISTS usertable ("
				+ "ID INT(8) NOT NULL, "
				+ "PASSWORD VARCHAR(30) NOT NULL, "
				+ "EMAIL VARCHAR(30) NOT NULL, "
				+ "FIRSTNAME VARCHAR(30) NOT NULL, "
				+ "LASTNAME VARCHAR(30) NOT NULL, "
				+ "TYPE CHAR(1) NOT NULL, "
				+ "PRIMARY KEY ( id ))");
		database.send("CREATE TABLE IF NOT EXISTS coursetable ("
				+ "ID INT(8) NOT NULL AUTO_INCREMENT, "
				+ "PROFID INT(8) NOT NULL, "
				+ "NAME VARCHAR(50) NOT NULL, "
				+ "ACTIVE BIT(1) NOT NULL, "
				+ "PRIMARY KEY ( id ))");
		database.send("CREATE TABLE IF NOT EXISTS studentenrollmenttable ("
				+ "ID INT(8) NOT NULL AUTO_INCREMENT, "
				+ "STUDENTID INT(8) NOT NULL, "
				+ "COURSEID INT(8) NOT NULL, "
				+ "PRIMARY KEY ( id ))");
		database.send("CREATE TABLE IF NOT EXISTS assignmenttable ("
				+ "ID INT(8) NOT NULL AUTO_INCREMENT, "
				+ "COURSEID INT(8) NOT NULL, "
				+ "TITLE VARCHAR(50) NOT NULL, "
				+ "PATH VARCHAR(100) NOT NULL, "
				+ "ACTIVE BIT(1) NOT NULL, "
				+ "DUEDATE CHAR(16) NOT NULL, "
				+ "PRIMARY KEY ( id ))");
		database.send("CREATE TABLE IF NOT EXISTS submissiontable ("
				+ "ID INT(8) NOT NULL AUTO_INCREMENT, "
				+ "ASSIGNID INT(8) NOT NULL, "
				+ "STUDENTID INT(8) NOT NULL, "
				+ "PATH VARCHAR(100) NOT NULL, "
				+ "TITLE VARCHAR(50) NOT NULL, "
				+ "SUBMISSIONGRADE INT(3) NOT NULL, "
				+ "COMMENTS VARCHAR(140) NOT NULL, "
				+ "TIMESTAMP CHAR(16) NOT NULL, "
				+ "PRIMARY KEY ( id ))");
		database.send("CREATE TABLE IF NOT EXISTS gradetable ("
				+ "ID INT(8) NOT NULL, "
				+ "ASSIGNID INT(8) NOT NULL, "
				+ "STUDENTID INT(8) NOT NULL, "
				+ "COURSEID INT(8) NOT NULL, "
				+ "ASSIGNMENTGRADE INT(3) NOT NULL, "
				+ "PRIMARY KEY ( id ))");
	}
	
	/**
	 * basic runnable waits for data to be written to socket
	 * and then passes the according function depending on the
	 * type of object passed and the object's action id. 
	 */
	@Override
	public void run() {
		checkTables();
		Object o = null;
		while(true) {
			o = null;
			try {
				o = com.getQuery();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SocketException e) {
				com.close();
				return;
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(o instanceof User) {
				User a = (User) o;
				if(a.actionID() == 0) {
					Login(a);
				} else if(a.actionID() == 1) {
					if(type == 'p')
						BrowseCoursesP(a);
					else if(type == 's')
						BrowseCoursesS(a);
				} else if(a.actionID() == 2) {
					searchStudent(a, false);
				} else if(a.actionID() == 3) {
					searchStudent(a, true);
				}else {
					
				}
			} else if (o instanceof Course) {
				Course a = (Course) o;
				if(a.actionID() == 0) {
					ManageCourse(a);
				} else if(a.actionID() == 1) {
					BrowseStudents(a);
				} else if(a.actionID() == 2) {
					BrowseAssignments(a);
				} else if(a.actionID() == 3) {
					BrowseGrades(a);
				} else if (a.actionID() == 4) {
					AddCourse(a);
				}else {
					
				}
			} else if (o instanceof Enrollment) {
				Enrollment a = (Enrollment) o;
				if(a.actionID() == 0) {
					ManageStudent(a, true);
				} else if(a.actionID() == 1) {
					ManageStudent(a, false);
				} else {
					
				}
			} else if (o instanceof Assignment) {
				Assignment a = (Assignment) o;
				if(a.actionID() == 0) {
					ManageAssignment(a);
				} else if(a.actionID() == 1) {
					BrowseSubmissions(a);
				} else if(a.actionID() == 2) {
					//BrowseGrades(a);
				} else if(a.actionID() == 3) {
					AddAssignment(a);
				} else if(a.actionID() == 4) {
					DownloadAssignment(a);
				} else {
					
				}
			} else if (o instanceof Submission) {
				Submission a = (Submission) o;
				if(a.actionID() == 0) {
					AddAssignmentSubmission(a);
				} else if (a.actionID() == 1) {
					DownloadSubmission(a);
				} else if (a.actionID() == 2){
					GradeSubmission(a);
				} else {
					
				}
			} else if (o instanceof Email) {
				Email a = (Email) o;
				if(this.type == 'p')
					EmailP(a);
				else if(this.type == 's')
					EmailS(a);
			} else {
				
			}
		}
	}

	/**
	 * login method deals with checking user id and 
	 * setting up some local variables. 
	 * @param u user object
	 */
	public void Login(User u) {
		ResultSet r = database.query("SELECT * FROM usertable WHERE ID = " + u.ID() + ";");
		String pw = null;
		String e = null;
		char t = 'u';
		try {
			while(r.next()) {
				pw = r.getString("PASSWORD");
				t = r.getString("TYPE").charAt(0);
				e = r.getString("EMAIL");
			}
			r.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		if(pw == null) {
			u.setActionID(1);
		} else if(!pw.equals(u.password())) {
			u.setActionID(1);
		} else {
			u.setActionID(0);
			u.setType(t);
			type = t;
			this.email = e;
		}
		try {
			com.sendQuery(u);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * browse course method deals with a query
	 * to view all courses that a student
	 * is associated with
	 * @param u user object
	 */
	public void BrowseCoursesS(User u) {
		ResultSet r = database.query("SELECT * FROM studentenrollmenttable WHERE STUDENTID = " + u.ID() + ";");
		Queue<Integer> i = new LinkedList<Integer>();
		ArrayList<Course> clist = new ArrayList<Course>();
		try {
			while(r.next())
				i.add(r.getInt("COURSEID"));
			while(!i.isEmpty()) {
				r = database.query("SELECT * FROM coursetable WHERE ID = " + i.remove() + " AND ACTIVE = 1;");
				while(r.next())
					clist.add(new Course(r.getInt("ID"), r.getInt("PROFID"), r.getString("NAME"), r.getBoolean("ACTIVE"), 0));
			}
			r.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			com.sendQuery(clist.toArray());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * browse course method deals with a query
	 * to view all courses that a professor
	 * is associated with
	 * @param u user object
	 */
	public void BrowseCoursesP(User u) {
		ResultSet r = database.query("SELECT * FROM coursetable WHERE PROFID = " + u.ID() + ";");
		ArrayList<Course> clist = new ArrayList<Course>();
		try {
			while(r.next())
				clist.add(new Course(r.getInt("ID"), r.getInt("PROFID"), r.getString("NAME"), r.getBoolean("ACTIVE"), 0));
			r.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			com.sendQuery(clist.toArray());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * inserts a new course into the database. 
	 * @param c course object
	 */
	public void AddCourse(Course c) {
		if(type != 'p')
			return;
		database.send("INSERT INTO coursetable (PROFID, NAME, ACTIVE) VALUES (" + c.profID() + ",'" + c.name() + "'," + c.active() + ");");
		File dir = new File("serverAssignments/" + c.ID());
		dir.mkdirs();
	}
	
	/**
	 * allows user to toggle active on a course. 
	 * @param c course object
	 */
	public void ManageCourse(Course c) {
		if(type != 'p')
			return;
		database.send("UPDATE coursetable SET ACTIVE = " + c.active() + " WHERE ID = " + c.ID() + ";");
	}
	
	/**
	 * allows user to add a new assignment to a course,
	 * taking in a file and creating a new set of local
	 * folders to hold files for assignment in. 
	 * @param a assignment object
	 */
	public void AddAssignment(Assignment a) {
		if(type != 'p')
			return;
		byte[] content = null;
		try {
			content = (byte[]) com.getQuery();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		File newFile = new File("serverAssignments/" + a.courseID() + "/" + a.title()); 
		newFile.mkdirs();
		newFile = new File("serverAssignments/" + a.courseID() + "/" + a.title() + "/Submissions");
		newFile.mkdirs();
		newFile = new File("serverAssignments/" + a.courseID() + "/" + a.title()); 
		newFile = new File(newFile.getPath() + "/" + a.title() + a.fileType());
		try {
			if(!newFile.exists())
				newFile.createNewFile();
			FileOutputStream writer = new FileOutputStream(newFile);
			BufferedOutputStream bos = new BufferedOutputStream(writer);
			bos.write(content);
			bos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		database.send("INSERT INTO assignmenttable (COURSEID, TITLE, PATH, ACTIVE, DUEDATE) VALUES (" + a.courseID() + ",'" + a.title() + "','serverAssignments/" + a.courseID() + "/" + a.title() + "/" + a.title() + a.fileType() + "'," + a.active() + ",'"+ a.dueDate() + "');");
	}
	
	/**
	 * method to add a new submission to an assignment
	 * places both a location on database and file in folders. 
	 * @param s submission object
	 */
	public void AddAssignmentSubmission(Submission s) {
		if(type != 's')
			return;
		byte[] content = null;
		try {
			content = (byte[]) com.getQuery();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		ResultSet r = database.query("SELECT * FROM assignmenttable WHERE ID = " + s.assignID() + ";");
		ArrayList<String> clist = new ArrayList<String>();
		try {
			while(r.next()) {
				clist.add(r.getString("COURSEID"));
				clist.add(r.getString("TITLE"));
			}
			r.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		File newFile = new File("serverAssignments/" + clist.get(0) + "/" + clist.get(1) + "/Submissions/" + s.studentID());
		newFile.mkdirs();
		newFile = new File(newFile.getPath() + "/" + s.title() + s.fileType()); // TODO modularize to have different file extensions (add parameter to assignment class)
		try {
			if(!newFile.exists())
				newFile.createNewFile();
			FileOutputStream writer = new FileOutputStream(newFile);
			BufferedOutputStream bos = new BufferedOutputStream(writer);
			bos.write(content);
			bos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		r = database.query("SELECT * FROM submissiontable WHERE ASSIGNID = " + s.assignID() + " AND STUDENTID = " + s.studentID() + ";");
		ArrayList<Integer> ilist = new ArrayList<Integer>();
		try {
			while(r.next())
				ilist.add(r.getInt("ID"));
			r.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(ilist.size() == 0)
			database.send("INSERT INTO submissiontable (ASSIGNID, STUDENTID, TITLE, PATH, SUBMISSIONGRADE, COMMENTS, TIMESTAMP) VALUES (" + s.assignID() + ",'" + s.studentID() + "','" + s.title() + "','" + "serverAssignments/" + clist.get(0) + "/" + clist.get(1) + "/Submissions/" + s.studentID() + "/" + s.title() + s.fileType() + "', -1, '', '');");
		else
			database.send("UPDATE submissiontable SET TITLE = '" + s.title() + "', PATH = '" + "serverAssignments/" + clist.get(0) + "/" + clist.get(1) + "/Submissions/" + s.studentID() + "/" + s.title() + s.fileType() + "', SUBMISSIONGRADE = -1, COMMENTS = '' WHERE ID = " + ilist.get(0) + ";");
	}
	
	/**
	 * method allows a student to download a specific file 
	 * associated with an assignment to a local folder. 
	 * @param a assignment object
	 */
	private void DownloadAssignment(Assignment a) {
		ResultSet r = database.query("SELECT * FROM assignmenttable WHERE ID = " + a.ID() + ";");
		ArrayList<String> clist = new ArrayList<String>();
		try {
			while(r.next()) {
				clist.add(r.getString("PATH"));
				clist.add(r.getString("TITLE"));
			}
			r.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		File file = new File(clist.get(0));
		if(!file.exists())
			return; // TODO deal with this
		long length = file.length();
		byte[] content = new byte[(int) length];
		try {
			BufferedInputStream b = new BufferedInputStream(new FileInputStream(file));
			b.read(content, 0, (int) length);
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		Assignment assignment = new Assignment(1, 1, clist.get(1), "", false, "", 0);
		String s = clist.get(0);
		s = s.substring(s.length()-4);
		assignment.setFileType(s);
		try {
			com.sendQuery(assignment);
			com.sendQuery(content);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * method for a professor to download a submission
	 * to a local folder. 
	 * @param a submission object
	 */
	private void DownloadSubmission(Submission a) {
		ResultSet r = database.query("SELECT * FROM submissiontable WHERE ID = " + a.ID() + ";");
		ArrayList<String> clist = new ArrayList<String>();
		try {
			while(r.next()) {
				clist.add(r.getString("PATH"));
				clist.add(r.getString("TITLE"));
			}
			r.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		File file = new File(clist.get(0));
		if(!file.exists())
			return; // TODO deal with this
		long length = file.length();
		byte[] content = new byte[(int) length];
		try {
			BufferedInputStream b = new BufferedInputStream(new FileInputStream(file));
			b.read(content, 0, (int) length);
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		Submission submission = new Submission(0, 1, 1, clist.get(1), "", 0, "", "", 0);
		String s = clist.get(0);
		s = s.substring(s.length()-4);
		submission.setFileType(s);
		try {
			com.sendQuery(submission);
			com.sendQuery(content);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * allows teacher to update grade associated with a submission. 
	 * @param a submission object
	 */
	private void GradeSubmission(Submission a) {
		database.send("UPDATE submissiontable SET SUBMISSIONGRADE = " + a.grade() + ", COMMENTS = '" + a.comments() + "' WHERE ID = " + a.ID() + ";");
	}
	
	/**
	 * method to display the set of grades for all assignments of a course
	 * to a student. 
	 * @param a course object 
	 */
	private void BrowseGrades(Course a) {
		ResultSet r = database.query("SELECT * FROM assignmenttable WHERE COURSEID = " + a.ID() + ";");
		Queue<Integer> i = new LinkedList<Integer>();
		ArrayList<Submission> clist = new ArrayList<Submission>();
		try {
			while(r.next()) {
				int id = r.getInt("ID");
				String title = r.getString("TITLE");
				int grade = -1;
				String comments = "";
				ResultSet r2 = database.query("SELECT * FROM submissiontable WHERE STUDENTID = " + a.profID() + " AND ASSIGNID = " + id + ";");
				while(r2.next()) {
					grade = r2.getInt("SUBMISSIONGRADE");
					comments = r2.getString("COMMENTS");
				}
				r2.close();
				clist.add(new Submission(0, 0, 0, title, "", grade, comments, "", 0));
			}
			r.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			com.sendQuery(clist.toArray());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * method to allow teachers to look at a list of all
	 * students, both enrolled and unenrolled. 
	 * @param c course object 
	 */
	public void BrowseStudents(Course c) {
		if(type != 'p')
			return;
		ArrayList<User> clist = new ArrayList<User>();
		ResultSet r = database.query("SELECT * FROM studentenrollmenttable WHERE COURSEID = " + c.ID() + ";");
		ArrayList<Integer> inarr = new ArrayList<Integer>();
		try {
			while(r.next()) {
				inarr.add(r.getInt("STUDENTID"));
			}
			r = database.query("SELECT * FROM usertable WHERE TYPE = 's';"); // TODO make sure good syntax
			while(r.next()) {
			for(int i = 0; i <= inarr.size(); i++) {
				if(i == inarr.size()) {
					clist.add(new User(r.getInt("ID"), r.getString("EMAIL"), null, r.getString("FIRSTNAME"), r.getString("LASTNAME"), r.getString("TYPE").charAt(0), 0));
					break;
				} else if(r.getInt("ID") == inarr.get(i)) {
					clist.add(new User(r.getInt("ID"), r.getString("EMAIL"), null, r.getString("FIRSTNAME"), r.getString("LASTNAME"), r.getString("TYPE").charAt(0), 1));
					break;
				}
			}
			}
			r.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			com.sendQuery(clist.toArray());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * method to search for a student from the list of students
	 * either by lastname or by firstname. 
	 * @param s user object
	 * @param b id vs name
	 */
	public void searchStudent(User s, boolean b) {
		
		ResultSet r = null;
		if(b)
			r = database.query("SELECT * FROM usertable WHERE ID = " + s.ID() + ";");
		else
			r = database.query("SELECT * FROM usertable WHERE LASTNAME = '" + s.lastName() + "';");
		ArrayList<User> clist = new ArrayList<User>();
		try {
			while(r.next())
				clist.add(new User(r.getInt("ID"), null, r.getString("EMAIL"), r.getString("FIRSTNAME"), r.getString("LASTNAME"), r.getString("TYPE").charAt(0), 0));
			r.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			com.sendQuery(clist.toArray());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * method to enroll or unenroll a student from a course. 
	 * @param e enrollment object
	 * @param b	enroll or unenroll
	 */
	public void ManageStudent(Enrollment e, boolean b) {
		if(type != 'p')
			return;
		if(b) {
			database.send("INSERT INTO studentenrollmenttable (COURSEID, STUDENTID) VALUES (" + e.courseID() + "," + e.studentID() + ");");
		} else {
			database.send("DELETE FROM studentenrollmenttable WHERE STUDENTID = " + e.studentID() + " AND COURSEID = " + e.courseID() + ";");
		}
	}
	
	/**
	 * method to allow user to look through all assignments of a course. 
	 * @param c course object
	 */
	public void BrowseAssignments(Course c) {
		ResultSet r = database.query("SELECT * FROM assignmenttable WHERE COURSEID = " + c.ID() + ";");
		ArrayList<Assignment> clist = new ArrayList<Assignment>();
		try {
			while(r.next())
				clist.add(new Assignment(r.getInt("ID"), r.getInt("COURSEID"), r.getString("TITLE"), null, r.getBoolean("ACTIVE"), r.getString("DUEDATE"), 0));
			r.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			com.sendQuery(clist.toArray());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * method to allow a professor to set an assignment to
	 * active or inactive. 
	 * @param a assignment
	 */
	public void ManageAssignment(Assignment a) {
		if(type != 'p')
			return;
		database.send("UPDATE assignmenttable SET ACTIVE = " + a.active() + " WHERE ID = " + a.ID() + ";");
	}
	
	/**
	 * method to allow a professor to send an email to 
	 * all students enrolled in their course. 
	 * @param e email object
	 */
	public void EmailP(Email e) {
		Course c = null;
		try {
			c = (Course) com.getQuery();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (SocketException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		ArrayList<String> slist = new ArrayList<String>();
		ResultSet r = database.query("SELECT * FROM studentenrollmenttable WHERE COURSEID = " + c.ID() + ";");
		ArrayList<Integer> inarr = new ArrayList<Integer>();
		try {
			while(r.next()) {
				inarr.add(r.getInt("STUDENTID"));
			}
			r = database.query("SELECT * FROM usertable WHERE TYPE = 's';"); // TODO make sure good syntax
			while(r.next()) {
			for(int i = 0; i < inarr.size(); i++) {
				if(r.getInt("ID") == inarr.get(i)) {
					slist.add(r.getString("EMAIL"));
					break;
				}
			}
			}
			r.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String[] slistarr = new String[slist.size()];
		for(int i = 0; i < slist.size(); i++) {
			slistarr[i] = (String) slist.get(i);
		}
		sendEmail(this.email, e.password(), slistarr, e.subject(), e.content(), false, false);
	}
	
	/**
	 * method to allow a student to send an email to
	 * the professor of their course. 
	 * @param e email object
	 */
	public void EmailS(Email e) {
		Course c = null;
		try {
			c = (Course) com.getQuery();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (SocketException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		ArrayList<String> slist = new ArrayList<String>();
		ResultSet r = database.query("SELECT * FROM usertable WHERE ID = " + c.profID() + ";");
		try {
			while(r.next()) {
				slist.add(r.getString("EMAIL"));
			}
			r.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String[] slistarr = new String[slist.size()];
		for(int i = 0; i < slist.size(); i++) {
			slistarr[i] = (String) slist.get(i);
		}
		sendEmail(this.email, e.password(), slistarr, e.subject(), e.content(), false, false);
	}
	
	/**
	 * method to allow a professor to browse through all submissions made to 
	 * a particular assignment. 
	 * @param a assignment object 
	 */
	public void BrowseSubmissions(Assignment a) {
		if(type != 'p')
			return;
		ArrayList<Submission> clist = new ArrayList<Submission>();
		ResultSet r = database.query("SELECT * FROM submissiontable WHERE ASSIGNID = " + a.ID() + ";");
		try {
			while(r.next()) {
				clist.add(new Submission(r.getInt("ID"), r.getInt("ASSIGNID"), r.getInt("STUDENTID"), r.getString("TITLE"), null, r.getInt("SUBMISSIONGRADE"), null, r.getString("TIMESTAMP"), 0));
			}
			r.close();
		} catch (SQLException e2) {
			e2.printStackTrace();
		}
		try {
			com.sendQuery(clist.toArray());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Function to send an email to multiple receivers. The emails must be from gmaill.com 
	 * @param YOUR_ADDRESS Sender address
	 * @param YOUR_PASSWORD Sender Email password
	 * @param RECIPIENT_ADDRESS An array of email address to be sent to.
	 * @param SUBJECT Subject of the email
	 * @param CONTENT Content of the email
	 * @param BCC Option to his who was sent to.
	 * @param DEBUG Enables debug mode
	 * @return if the email successfully sent or not
	 */
	public static boolean sendEmail(String YOUR_ADDRESS, String YOUR_PASSWORD, String[] RECIPIENT_ADDRESS, String SUBJECT, String CONTENT, boolean BCC, boolean DEBUG) {
		try {
			if(!YOUR_ADDRESS.endsWith("@gmail.com"))
				throw new EmailException("Detected an address of sender not of @gmail.com: " + YOUR_ADDRESS + "\n");
			for(int i = 0 ; i < RECIPIENT_ADDRESS.length ; i++)
				if(!RECIPIENT_ADDRESS[i].endsWith("@gmail.com"))
					throw new EmailException("Detected an address of reciver not of @gmail.com: " + RECIPIENT_ADDRESS[i] + "\n");
			MultiPartEmail email = new MultiPartEmail();
			email.setSmtpPort(587);
			email.setAuthenticator(new DefaultAuthenticator(YOUR_ADDRESS, YOUR_PASSWORD));
			email.setSSLOnConnect(true);
			email.setDebug(DEBUG);
			email.setHostName("smtp.gmail.com");
			email.setFrom(YOUR_ADDRESS);
			email.setSubject(SUBJECT);
			email.setMsg(CONTENT);
			if(BCC)	email.addBcc(RECIPIENT_ADDRESS);
			else	email.addTo(RECIPIENT_ADDRESS);
			email.send();
			if(DEBUG)	System.out.println("Mail sent!"); 
			return true;
		}catch (EmailException e) {
			if(DEBUG)	e.printStackTrace();
			return false;
		}
	}
}
