package main.BackEnd;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.net.SocketException;

/**
 * A class that takes Serializable objects and sends them over the socket to the client and receives objects.
 * @author Matthew Wiens
 * @author Taylor Huang
 * @author Jeffrey Layton
 */
public class Communicator {
	
	/**
	* The socket connection that will pair to the server.
	*/
	private Socket socket;
	
	/**
	* The writer to send to the server.
	*/
	private ObjectOutputStream socketOut;
	
	/**
	* The reader that receives content from the server.
	*/
	private ObjectInputStream socketIn;

	/**
	 * Basic assignment constructor
	 * @param s Socket to use
	 * @param Out ObjectOutputStream stream to send through the socket
	 * @param In ObjectInputStream stream to receive through the socket
	 */
	public Communicator(Socket s, ObjectOutputStream Out, ObjectInputStream In) {
		socket = s;
		socketIn = In;
		socketOut = Out;
	}
	
	/**
	 * Sends objects through the socket to the client
	 * @param o Object to be sent. Needs to be Serializable.
	 * @throws IOException Trouble writing to the writer.
	 */
	public void sendQuery(Object o) throws IOException {
		if(o instanceof Serializable) {
			socketOut.writeObject(o);
			socketOut.flush();
			socketOut.reset();
		}else {
			throw new IOException("Non-serializable exception\n");
		}
	}
	
	/**
	 * Receives Objects from the server
	 * @return Object Received
	 * @throws IOException Trouble when reading.
	 * @throws ClassNotFoundException If the class cannot be found
	 * @throws SocketException If the client disconnects
	 */
	public Object getQuery() throws IOException, ClassNotFoundException, SocketException {
		return (Object) socketIn.readObject();
	}
	
	/**
	 * Closes the socket, input, and output.
	 */
	public void close() {
		try {
			if(socketIn != null)
				socketIn.close();
			if(socketOut != null)
				socketOut.close();
			if(socket != null)
				socket.close();
		} catch(Exception e) {
			
		}
	}
}
