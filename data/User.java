
package main.data;

import java.io.Serializable;


/**
 * A class that holds information about a user. The user can be a professor or a student. 
 * @author Matthew Wiens
 * @author Taylor Huang
 * @author Jeffrey Layton
 */
public class User implements Serializable {
	
	/**
	 *  ID for User
	 */
  private int id;
  
	/**
	 *  Email address of the user
	 */
  private String email;
  
	/**
	 *  Password for the email address
	 */
  private String password;
  
	/**
	 *  First name of the user
	 */
  private String firstName;
  
	/**
	 *  Last name of the user
	 */
  private String lastName;
  
	/**
	 *  Char identifying type of user. 's' for Student and 'p' for Professor
	 */
  private char type;
  
	/**
	 *  ID for the action to be performed on the other side of the client- server connection
	 */
  private int actionID;

	/**
	 * Basic assignment constructor
	 * @param i User ID
	 * @param e Email address
	 * @param p Password for login
	 * @param f First Name of user
	 * @param l Last name of user
	 * @param t Type of user
	 * @param ai Action ID
	 */
  public User(int i, String e, String p, String f, String l, char t, int a) {
    id = i;
    email = e;
    password = p;
    firstName = f;
    lastName = l;
    type = t;
    actionID = a;
  }

	/**
	 * Grabs the User ID
	 * @return User ID
	 */
  public int ID() { return id; }
  
	/**
	 * Sets the User ID
	 * @param i User ID
	 */
  public void setID(int i) { id = i; }

	/**
	 * Grabs the Email Address
	 * @return Email Address
	 */
  public String email() { return email; }
  
	/**
	 * Sets the Email Address
	 * @param e Email Address
	 */
  public void setEmail(String e) { email = e; }

	/**
	 * Grabs the Password for Email
	 * @return Password for Email
	 */
  public String password() { return password; }
  
	/**
	 * Sets the Password for Email
	 * @param p Password for Email
	 */
  public void setPassword(String p) { password = p; }

	/**
	 * Grabs the First Name
	 * @return First Name
	 */
  public String firstName() { return firstName; }
  
	/**
	 * Sets the First Name
	 * @param f First Name
	 */
  public void setFirstName(String f) { firstName = f; }

	/**
	 * Grabs the Last Name
	 * @return Last Name
	 */
  public String lastName() { return lastName; }
  
	/**
	 * Sets the Last Name
	 * @param l Last Name
	 */
  public void setLastName(String l) { lastName = l; }

	/**
	 * Grabs the Type of User
	 * @return Type of User
	 */
  public char type() { return type; }
  
	/**
	 * Sets the Type of User
	 * @param t Type of User
	 */
  public void setType(char t) { type = t; }

	/**
	 * Grabs the Action ID
	 * @return Action ID
	 */
  public int actionID() { return actionID; }
  
	/**
	 * Sets the Action ID
	 * @param a Action ID
	 */
  public void setActionID(int a) { actionID = a; }
}
