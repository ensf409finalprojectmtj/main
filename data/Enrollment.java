
package main.data;

import java.io.Serializable;


/**
 * A class that holds information about an enrollment of a student in a course.
 * @author Matthew Wiens
 * @author Taylor Huang
 * @author Jeffrey Layton
 */
public class Enrollment implements Serializable {
	
	/**
	 *  ID for Enrollment
	 */
  private int id;
  
	/**
	 *  ID for the course being enrolled in
	 */
  private int courseID;
  
	/**
	 *  ID for the student that is enrolled
	 */
  private int studentID;
  
	/**
	 *  ID for the action to be performed on the other side of the client- server connection
	 */
  private int actionID;

	/**
	 * Basic assignment constructor
	 * @param i Enrollment ID
	 * @param c Course ID
	 * @param s Student ID
	 * @param a Action ID
	 */
  public Enrollment(int i, int c, int s, int a) {
    id = i;
    courseID = c;
    studentID = s;
    actionID = a;
  }

	/**
	 * Grabs the Enrollment ID
	 * @return Enrollment ID
	 */
  public int ID() { return id; }
  
	/**
	 * Sets the Enrollment ID
	 * @param i Enrollment ID
	 */
  public void setID(int i) { id = i; }

	/**
	 * Grabs the Course ID
	 * @return Course ID
	 */
  public int courseID() { return courseID; }
  
	/**
	 * Sets the Course ID
	 * @param c Course ID
	 */
  public void setCourseID(int c) { courseID = c; }

	/**
	 * Grabs the Student ID
	 * @return Student ID
	 */
  public int studentID() { return studentID; }
  
	/**
	 * Sets the Student ID
	 * @param s Student ID
	 */
  public void setStudentID(int s) { studentID = s; }

	/**
	 * Grabs the Action ID
	 * @return Action ID
	 */
  public int actionID() { return actionID; }
  
	/**
	 * Sets the Action ID
	 * @param a Action ID
	 */
  public void setActionID(int a) { actionID = a; }
}
