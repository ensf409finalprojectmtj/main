
package main.data;

import java.io.Serializable;


/**
 * A class that holds information about an assignment from a single course.
 * @author Matthew Wiens
 * @author Taylor Huang
 * @author Jeffrey Layton
 */
public class Assignment implements Serializable {
	
	/**
	 *  ID for the assignment
	 */
  private int id;
  
	/**
	 *  ID for the course it is a part of
	 */
  private int courseID;
  
	/**
	 *  Title of the assignment
	 */
  private String title;
  
	/**
	 *  Path to find the assignment document
	 */
  private String path;
  
	/**
	 *  Indicates if an assignment is active or not
	 */
  private boolean active;
  
	/**
	 *  String containing the data that the assignment is due
	 */
  private String dueDate;
  
	/**
	 *  ID for the action to be performed on the other side of the client- server connection
	 */
  private int actionID;
  
  private String filetype;

	/**
	 * Basic assignment constructor
	 * @param i Assignment ID
	 * @param c Course ID
	 * @param t Title
	 * @param p File Path
	 * @param a Activation Status
	 * @param d Due Date of Assignment
	 * @param ai Action ID
	 */
  public Assignment(int i, int c, String t, String p, boolean a, String d, int ai) {
    id = i;
    courseID = c;
    title = t;
    path = p;
    active = a;
    dueDate = d;
    actionID = ai;
  }

  public void setFileType(String type) { filetype = type; }
  
  public String fileType() { return filetype; }
  
	/**
	 * Grabs the Assignment ID
	 * @return Assignment ID
	 */
  public int ID() { return id; }
  
	/**
	 * Sets the Assignment ID
	 * @param i Assignment ID
	 */
  public void setID(int i) { id = i; }

	/**
	 * Grabs the Course ID
	 * @return Course ID
	 */
  public int courseID() { return courseID; }
  
	/**
	 * Sets the Course ID
	 * @param c Course ID
	 */
  public void setCourseID(int c) { courseID = c; }

	/**
	 * Grabs the Title
	 * @return Title
	 */
  public String title() { return title; }
  
	/**
	 * Sets the Title
	 * @param t Title
	 */
  public void setTitle(String t) { title = t; }

	/**
	 * Grabs the File Path
	 * @return File Path
	 */
  public String path() { return path; }
  
	/**
	 * Sets the File Path
	 * @param p File Path
	 */
  public void setPath(String p) { path = p; }

	/**
	 * Grabs the Active State
	 * @return Active State
	 */
  public boolean active() { return active; }
  
	/**
	 * Sets the Active State
	 * @param a Active State
	 */
  public void setActive(boolean a) { active = a; }

	/**
	 * Grabs the Due Date
	 * @return Due Date
	 */
  public String dueDate() { return dueDate; }
  
	/**
	 * Sets the Due Date
	 * @param d Due Date
	 */
  public void setDueDate(String d) { dueDate = d; }

	/**
	 * Grabs the Action ID
	 * @return Action ID
	 */
  public int actionID() { return actionID; }
  
	/**
	 * Sets the Action ID
	 * @param a Action ID
	 */
  public void setActionID(int a) { actionID = a; }
}
