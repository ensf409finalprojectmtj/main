
package main.data;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * A class that holds information about an active to be email sent.
 * @author Matthew Wiens
 * @author Taylor Huang
 * @author Jeffrey Layton
 */
public class Email implements Serializable {
	
	/**
	 * Email address of the sender
	 */
  private String from;
  
	/**
	 * Email address of all the receivers
	 */
  private ArrayList<String> to;
  
	/**
	 * Password of the sending email
	 */
  private String password;
  
	/**
	 * Subject of the email
	 */
  private String subject;
  
	/**
	 * Content of the email
	 */
  private String content;

	/**
	 * Basic assignment constructor
	 * @param f Sender Address
	 * @param t ArrayList of Receivers
	 * @param p Password to email account
	 * @param s Subject of email
	 * @param c Content of email
	 */
  public Email(String f, ArrayList<String> t, String p, String s, String c) {
    from = f;
    to = t;
    password = p;
    subject = s;
    content = c;
  }

	/**
	 * Grabs the Sender address
	 * @return Sender address
	 */
  public String from() { return from; }
  
	/**
	 * Sets the Sender address
	 * @param f Sender address
	 */
  public void setFrom(String f) { from = f; }

	/**
	 * Grabs the all the receivers
	 * @return all the receivers
	 */
  public ArrayList<String> to() { return to; }
  
	/**
	 * Sets the all the receivers
	 * @param t all the receivers
	 */
  public void setTo(ArrayList<String> t) { to = new ArrayList<String>(t); }

	/**
	 * Grabs the Subject of the email
	 * @return Subject of the email
	 */
  public String subject() { return subject; }
  
	/**
	 * Sets the Subject of the email
	 * @param s Subject of the email
	 */
  public void setSubject(String s) { subject = s; }

	/**
	 * Grabs the Content of the email
	 * @return Content of the email
	 */
  public String content() { return content; }
  
	/**
	 * Sets the Content of the email
	 * @param c Content of the email
	 */
  public void setContent(String c) { content = c; }

	/**
	 * Grabs the Password 
	 * @return Password 
	 */
  public String password() { return password; }
  
	/**
	 * Sets the Password 
	 * @param p Password 
	 */
  public void setPassword(String p) { password = p; }

}
