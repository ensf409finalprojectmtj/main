
package main.data;

import java.io.Serializable;


/**
 * A class that holds information about a course to a single professor.
 * @author Matthew Wiens
 * @author Taylor Huang
 * @author Jeffrey Layton
 */
public class Course implements Serializable {
	
	/**
	 *  ID for Course
	 */
  private int id;
  
	/**
	 *  ID for Professor controlling the course
	 */
  private int profID;
  
	/**
	 *  Name for the course
	 */
  private String name;
  
	/**
	 *  Activation state of the course
	 */
  private boolean active;
  
	/**
	 *  ID for the action to be performed on the other side of the client- server connection
	 */
  private int actionID;

	/**
	 * Basic assignment constructor
	 * @param i Course ID
	 * @param p Professor  ID
	 * @param n Name of Course
	 * @param a Active Status
	 * @param ai Action ID
	 */
  public Course(int i, int p, String n, boolean a, int ai) {
    id = i;
    profID = p;
    name = n;
    active = a;
    actionID = ai;
  }

	/**
	 * Grabs the Course ID
	 * @return Course ID
	 */
  public int ID() { return id; }
  
	/**
	 * Sets the Course ID
	 * @param i Course ID
	 */
  public void setID(int i) { id = i; }

	/**
	 * Grabs the Professor ID
	 * @return Professor ID
	 */
  public int profID() { return profID; }
  
	/**
	 * Sets the Professor ID
	 * @param p Professor ID
	 */
  public void setProfID(int p) { profID = p; }

	/**
	 * Grabs the Name of the Course
	 * @return Name of the Course
	 */
  public String name() { return name; }
  
	/**
	 * Sets the Name of the Course
	 * @param n Name of the Course
	 */
  public void setName(String n) { name = n; }

	/**
	 * Grabs the Active State of the course
	 * @return Active State of the course
	 */
  public boolean active() { return active; }
  
	/**
	 * Sets the Active State of the course
	 * @param a Active State of the course
	 */
  public void setActive(boolean a) { active = a; }
  
	/**
	 * Grabs the Action ID
	 * @return Action ID
	 */
  public int actionID() { return actionID; }
  
	/**
	 * Sets the Action ID
	 * @param a Action ID
	 */
  public void setActionID(int a) { actionID = a; }
}
