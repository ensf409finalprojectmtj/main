
package main.data;

import java.io.Serializable;


/**
 * A class that holds information of a grade to an assignment for a student.
 * @author Matthew Wiens
 * @author Taylor Huang
 * @author Jeffrey Layton
 */
public class Grade implements Serializable {
	
	/**
	 *  ID for Grade
	 */
  private int id;
  
	/**
	 *  ID for the Assignment getting graded
	 */
  private int assignID;
  
	/**
	 *  ID for the Student getting graded
	 */
  private int studentID;
  
	/**
	 *  ID for Course the assignment it is in
	 */
  private int courseID;
  
	/**
	 *  Number for the grade
	 */
  private int grade;
  
	/**
	 *  ID for the action to be performed on the other side of the client- server connection
	 */
  private int actionID;

	/**
	 * Basic assignment constructor
	 * @param i Grade ID
	 * @param a Assignment ID
	 * @param s Student ID
	 * @param c Course ID
	 * @param g Grade Number
	 * @param ai Action ID
	 */
  public Grade(int i, int a, int s, int c, int g, int ai) {
    id = i;
    assignID = a;
    studentID = s;
    courseID = c;
    grade = g;
    actionID = ai;
  }

  public int ID() { return id; }
  public void setID(int i) { id = i; }

  public int assignID() { return assignID; }
  public void setAssignID(int a) { assignID = a; }

  public int studentID() { return studentID; }
  public void setStudentID(int s) { studentID = s; }

  public int courseID() { return courseID; }
  public void setCourseID(int c) { courseID = c; }

  public int grade() { return grade; }
  public void setGrade(int g) { grade = g; }

  public int actionID() { return actionID; }
  public void setActionID(int a) { actionID = a; }
}
