
package main.data;

import java.io.Serializable;


/**
 * A class that holds information about a submission to an assignment. 
 * @author Matthew Wiens
 * @author Taylor Huang
 * @author Jeffrey Layton
 */
public class Submission implements Serializable {
	
	/**
	 *  ID for the Submission
	 */
  private int id;
  
	/**
	 *  ID for the Assignment being submitted too
	 */
  private int assignID;
	/**
	 *  ID for the student submitting
	 */
  private int studentID;
  
	/**
	 *  Title of the file submitted
	 */
  private String title;
  
	/**
	 *  Path to find the assignment document
	 */
  private String path;
  
	/**
	 *  Grade of Submission
	 */
  private int grade;
  
	/**
	 *  Comments on the Submission
	 */
  private String comments;
  
	/**
	 *  Time stamp of the Submission
	 */
  private String timestamp;
  
	/**
	 *  ID for the action to be performed on the other side of the client- server connection
	 */
  private int actionID;
  
  private String filetype;

	/**
	 * Basic Submission constructor
	 * @param i Submission ID
	 * @param c Assignment ID
	 * @param c Student ID
	 * @param t Title
	 * @param p File Path
	 * @param g Grade of the Submission
	 * @param c Comments on the Submission
	 * @param c Time Stamp of the Submission
	 * @param ai Action ID
	 */
  public Submission(int i, int a, int s, String t, String p, int g, String c, String ts, int ai) {
    id = i;
    assignID = a;
    studentID = s;
    title = t;
    path = p;
    grade = g;
    comments = c;
    timestamp = ts;
    actionID = ai;
  }

  public void setFileType(String type) { filetype = type; }
  
  public String fileType() { return filetype; }
  
	/**
	 * Grabs the Submission ID
	 * @return Submission ID
	 */
  public int ID() { return id; }
  
	/**
	 * Sets the Submission ID
	 * @param i Submission ID
	 */
  public void setID(int i) { id = i; }

	/**
	 * Grabs the Assignment ID
	 * @return Assignment ID
	 */
  public int assignID() { return assignID; }
  
	/**
	 * Sets the Assignment ID
	 * @param a Assignment ID
	 */
  public void setAssignID(int a) { assignID = a; }

	/**
	 * Grabs the Student ID
	 * @return Student ID
	 */
  public int studentID() { return studentID; }
  
	/**
	 * Sets the Student ID
	 * @param s Student ID
	 */
  public void setStudentID(int s) { studentID = s; }

	/**
	 * Grabs the Title
	 * @return Title
	 */
  public String title() { return title; }
  
	/**
	 * Sets the Title
	 * @param t Title
	 */
  public void setTitle(String t) { title = t; }

	/**
	 * Grabs the File Path
	 * @return File Path
	 */
  public String path() { return path; }
  
	/**
	 * Sets the File Path
	 * @param p File Path
	 */
  public void setPath(String p) { path = p; }

	/**
	 * Grabs the Grade Number
	 * @return Grade Number
	 */
  public int grade() { return grade; }
  
	/**
	 * Sets the Grade Number
	 * @param g Grade Number
	 */
  public void setGrade(int g) { grade = g; }
  
	/**
	 * Grabs the Comment
	 * @return Comment
	 */
  public String comments() { return comments; }
  
	/**
	 * Sets the Comment
	 * @param c Comment
	 */
  public void setComments(String c) { comments = c; }
  
	/**
	 * Grabs the Time Stamp
	 * @return Time Stamp
	 */
  public String timestamp() { return timestamp; }
  
	/**
	 * Sets the Time Stamp
	 * @param t Time Stamp
	 */
  public void setTimestamp(String t) { timestamp = t; }
  
	/**
	 * Grabs the Action ID
	 * @return Action ID
	 */
  public int actionID() { return actionID; }
  
	/**
	 * Sets the Action ID
	 * @param a Action ID
	 */
  public void setActionID(int a) { actionID = a; }
}
